console.log("hello world");
// var, let, const
//var :global and functional scope
// let,const :block scoped
if(true){
    let a =20;
    console.log(a);
}

//data types 
// premitive data types
let name ="JavaScript";
let id =513;
let coursecompletion =true;
let job =null;
let batch;
console.log(typeof(name));
console.log(typeof(id));
console.log(typeof(coursecompletion));
console.log(typeof(job));
console.log(typeof(batch));
// complex data types
let scores =[13,"hello",29,23,56]
console.log(scores[1]);

// looping statements :for loop,while loop,do while
for(let i=0;i<scores.length;i++){
    console.log(scores[i]);
}
console.log(scores.length);
console.log(scores)


//function --block of reusable code
function addition(){
    let result = 30+40;

    return result;
} //function declaration
console.log(addition());//function calling

let additional =addition()
console.log(addition());

function subtraction(num1,num2){
    return num1-num2
}
// console.log(subtraction(40,60));

// let num1 =prompt("Enter the value");
// alert("the value you entered is 100" +num1);
// console.log(num1);

//create a fn that shuld accept 2values from the user and print the max val

function MaxValue(a,b){
    
    return Math.max(a,b);
}
console.log(MaxValue(13,27));

function MinValue(a,b){
    
    return Math.min(a,b);
}
console.log(MinValue(13,27));
//find a min val between 3 values
// generate random number

let randomnumber =Math.round(Math.random()*10);
console.log(randomnumber);
console.log(Math.floor(23.13));
console.log(Math.ceil(23.13));
console.log(Math.abs(23,13));
console.log(Math.pow(3,6));
console.log(Math.sqrt(72));

//find even num,odd num between 1-100
//print even num,odd num between 1-50
//what is server
//crate a git account
let i = 1;
for (i=1;i<= 100;i++){
    if(i%2==0){
        console.log(i + "is even")
    }
    
}

for(let a=0;a<=50;a++){
if(a%2!==0){
    console.log(a +"is odd");
}
}

function findEvenandOdd(num1){
    if (num1%2 ==0){
        console.log(num1 +"even number" )
    }else{
        console.log(num1 +"odd number")
    }
}
